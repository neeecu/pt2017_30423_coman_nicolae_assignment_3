package BLL;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import BLLValidators.EmailValidator;
import BLLValidators.Validator;
import DAO.CustomerDAO;

import MODEL.Customer;

public class CustomerBLL {

private List <Validator<Customer>> validators;
	
	
	public CustomerBLL(){
		
	validators = new ArrayList<Validator<Customer>>();	
	validators.add(new EmailValidator());	
		
	}
	
	
	public Customer findCustomerByID( int id)
	{
		Customer c1 = CustomerDAO.findById(id);
		if(c1==null)
		{
			
			throw new NoSuchElementException("the customer with id ="+id+" was not found");
		}
		return c1;
		
	}
	
	public Customer findCustomerByName(String name)
	{
		Customer c1 = CustomerDAO.findByName(name);
		
		if(c1==null)
			throw new NoSuchElementException("the customer with name "+name+" does not exist");
		
		return c1;
		
	}
	
	public int insertCustomer( Customer c1)
	{
		for(Validator<Customer> v : validators)
			v.validate(c1);
		
		return CustomerDAO.insert(c1);
		
	}
	
	
	public void  deleteCustomer(int ID)
	{
		
		if(findCustomerByID(ID)!=null)
		 CustomerDAO.delete(ID);
		else
			throw new NoSuchElementException("the customer with id ="+ID+" does not exist");
	}
	
	public void updateCustomer(int ID, Object updateName, Object updateAddress, Object updateEmail)
	{
		if(findCustomerByID(ID)!=null)
			CustomerDAO.update(ID,  updateName, updateAddress, updateEmail);
		else
			throw new NoSuchElementException("the product with id="+ID+" does not exist");
		
		
		
	}
	
}
