package BLL;



import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import DAO.OrderDAO;
import MODEL.orders;
import BLLValidators.QuantityValidator;
import BLLValidators.Validator;


public class OrderBLL {

	private List <Validator<orders>> validators;
	
	public OrderBLL(){
		validators = new ArrayList<Validator<orders>>();
		validators.add(new QuantityValidator());
	}
	
	public orders findOrderbyID(int id)
	{
		orders ord = OrderDAO.findById(id);
		if(ord==null)
			throw new NoSuchElementException("the order with id="+id+"  does not exist");
		return ord;
		
	}
	
	public int insertOrder(orders ord)
	{
		for(Validator<orders> v:validators)
			v.validate(ord);
		return OrderDAO.insert(ord);
	}
	
	public void updateOrder(int ID, Object customerName, Object productName, int quantity)
	{
		if(findOrderbyID(ID)!=null)
			OrderDAO.update(ID,customerName,productName,quantity);
		else
			throw new NoSuchElementException("the order with id="+ID+"  does not exist");
		
	}
	
	
}
