package BLL;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import DAO.ProductDAO;
import MODEL.Product;
import BLLValidators.PriceValidator;
import BLLValidators.Validator;

public class ProductBLL {


	private List <Validator<Product>> validators;
	
	
	public ProductBLL(){
		
	validators = new ArrayList<Validator<Product>>();	
	validators.add(new PriceValidator());	
		
	}
	
	
	public Product findProductByID( int id)
	{
		Product prod = ProductDAO.findById(id);
		if(prod==null)
		{
			
			throw new NoSuchElementException("the product with id ="+id+" was not found");
		}
		return prod;
		
	}
	
	public Product findProductByName(String name)
	{
		Product prod = ProductDAO.findByName(name);
		
		if(prod==null)
			throw new NoSuchElementException("the product with name "+name+" does not exist");
		
		return prod;
		
	}
	
	public int insertProduct( Product product)
	{
		for(Validator<Product> v : validators)
			v.validate(product);
		
		return ProductDAO.insert(product);
		
	}
	
	
	public void  deleteProduct(int ID)
	{
		
		if(findProductByID(ID)!=null)
		 ProductDAO.delete(ID);
		else
			throw new NoSuchElementException("the product with id ="+ID+" does not exist");
	}
	
	public void updateProduct(int ID, Object updateName, Object updateStock, Object updatePrice)
	{
		if(findProductByID(ID)!=null)
			ProductDAO.update(ID,  updateName, updateStock, updatePrice);
		else
			throw new NoSuchElementException("the product with id="+ID+" does not exist");
		
		
		
	}
	
	
}
