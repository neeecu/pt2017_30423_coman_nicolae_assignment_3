package BLLValidators;

import BLLValidators.Validator;
import MODEL.Product;

 
public class PriceValidator  implements Validator<Product> {
 
 

	@Override
	public void validate(Product t) {
	 if(t.getPrice()<=0 )
		 throw new IllegalArgumentException("The product must have a price greater than 0");
		
	}

}