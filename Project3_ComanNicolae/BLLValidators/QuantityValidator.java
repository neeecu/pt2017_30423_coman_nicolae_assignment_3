package BLLValidators;

import BLLValidators.Validator;
import MODEL.orders;

 
public class QuantityValidator  implements Validator<orders> {
 
 

	@Override
	public void validate(orders t) {
	 if(t.getQuantity()<=0 )
		 throw new IllegalArgumentException("The order quantity must be greater than 0");
		
	}

}