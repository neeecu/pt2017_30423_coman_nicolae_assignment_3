package BLLValidators;

public interface Validator<T> {

	public void validate(T t);
}
