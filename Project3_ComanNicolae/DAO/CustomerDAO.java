package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import java.io.FileOutputStream;
import java.util.Date;
import java.io.FileOutputStream;
import java.util.Date;
import java.io.FileOutputStream;
import java.util.Date;


 

import Connection.ConnectionFactory;
import GUI.CustomerGUI;
import GUI.GUI;
import GUI.OrderGUI;
import MODEL.Customer;
 

 
public class CustomerDAO {

	 static final Logger LOGGER = Logger.getLogger(Customer.class.getName());
	
	private static final String insertStatementString = "INSERT INTO customer (customerName,customerEmail,customerAddress)"
			+ " VALUES (?,?,?)";
	private static final String deleteStatementString = "DELETE  FROM customer where customerID = ?";
	
	private final static String findStatementString = "SELECT * FROM customer where customerID = ?";
	
	private final static String findByNameStatementString="Select *from customer where customerName = ?";
	
	private final static String updateStatementString ="UPDATE customer SET customerName = ?, customerEmail = ?, customerAddress = ? WHERE customerID = ?";
	
	private final static String resetAutoIncrement= "ALTER TABLE customer AUTO_INCREMENT = 1;";
	
	private final static String dropTableString="DROP TABLE customers";
	
	public static Customer findById(int customerId) {
		Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, customerId);
			rs = findStatement.executeQuery();
			rs.next();

			String customerName = rs.getString("customerName");
			String customerEmail = rs.getString("CustomerEmail");
			String customerAddress = rs.getString("CustomerAddress");
		
		 
			toReturn = new Customer(customerId, customerName, customerAddress, customerEmail);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	
	public static Customer findByName(String customerName) {
		Customer toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findByNameStatementString);
			findStatement.setString(1, customerName);
			rs = findStatement.executeQuery();
			rs.next();

			int customerID = rs.getInt("customerID");
			String customerEmail = rs.getString("CustomerEmail");
			String customerAddress = rs.getString("CustomerAddress");
		 
			toReturn = new Customer(customerID, customerName, customerEmail, customerAddress);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"CustomerDAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	
	
	
	public static int insert(Customer customer) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId=-1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, customer.getName());
			insertStatement.setString(2, customer.getAddress());
			insertStatement.setString(3, customer.getEmail());
			
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
				
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "CustomerDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	
	public static void delete(int ID)
	{
		 
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		
		try{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setLong(1,ID);
			deleteStatement.executeUpdate();
			CustomerDAO.updateIncrement();
		}
		catch(SQLException e)
		{
			LOGGER.log(Level.WARNING, "CustomerDAO:delete " + e.getMessage());
			
		}
		
		finally{
			
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
			
		}
	}
	
	public static void update ( int ID, Object updateName, Object updateAddress, Object updateEmail)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		Customer old = findById(ID);

		try{
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setLong(4,ID);
			
			if(updateName==null)
				updateStatement.setString(1,old.getName());
			else
				updateStatement.setString(1,(String)updateName);
			
			if(updateAddress==null)
				updateStatement.setString(2,old.getAddress());
			else
				updateStatement.setString(2,(String)updateAddress);
			if(updateEmail==null)
				updateStatement.setString(3,old.getEmail());
			else
				updateStatement.setString(3,(String)updateEmail);
			updateStatement.executeUpdate();
		 
		}
		catch(SQLException e)
		{
			LOGGER.log(Level.WARNING, "CustomerDAO:delete " + e.getMessage());
			
		}
		
		finally{
			
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
			
		}
		
		
	}
	
	
	
	public static void updateIncrement()
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateIncrement =null;
		
		try{
			updateIncrement = dbConnection.prepareStatement(resetAutoIncrement);
			updateIncrement.executeUpdate();

			
			
		}
		 catch(Exception e){}
		finally
		{
			ConnectionFactory.close(updateIncrement);
			ConnectionFactory.close(dbConnection);

			
		}
		
	}
	
	public static void fillComboBox()
	{
		Connection dbConnection=ConnectionFactory.getConnection();
		
		String querry="SELECT *  from customer";
		ResultSet rs = null;
		try{
		
			Statement statement =dbConnection.createStatement();

			rs =statement.executeQuery(querry);
		
			while(rs.next())
			{
				String name = "customerName";
				OrderGUI.customerComboBox.addItem(rs.getString(name));
				
			}
		
		}
		catch (SQLException e)
		{
			
			LOGGER.log(Level.WARNING, "CustomerDAO:fillComboBox " + e.getMessage());

			
		}
		
		finally{
			
			ConnectionFactory.close(dbConnection);
			
		}
	
	}
	public static void fillJTable()
	{
		
		
		Connection dbConnection=ConnectionFactory.getConnection();
		String querry="SELECT *  from customer";
		ResultSet rs = null;
		try{
			
			Statement statement =dbConnection.createStatement();

			rs =statement.executeQuery(querry);
		
			while(rs.next())
			{
				int id = rs.getInt("customerID");
				String name = rs.getString("customerName");
				String address= rs.getString("customerAddress");
				String email = rs.getString("customerEmail");
				CustomerGUI.model.addRow(new Object[]{id,name,email,address});
				
			}
		
		}
		catch (SQLException e)
		{
			
			LOGGER.log(Level.WARNING, "CustomerDAO:fillJtable " + e.getMessage());

			
		}
		
		finally{
			
			ConnectionFactory.close(dbConnection);
			
		}
	
		
	}
	
	
	
	
}
	
	

