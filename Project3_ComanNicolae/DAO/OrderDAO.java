package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectionFactory;
import MODEL.Product;
import MODEL.orders;
import GUI.GUI;

 
 

public class OrderDAO {

	protected static final Logger LOGGER = Logger.getLogger(orders.class.getName());
	
	private static final String insertStatementString = "INSERT INTO orders (customerName,productName,productQuantity)"
			+ " VALUES (?,?,?)";
	 
	
	private final static String findStatementString = "SELECT * FROM orders where orderID = ?";
	
	private final static String updateStatementString="UPDATE orders SET customerName = ?, productName = ?, productQuantity = ? WHERE orderID = ?";
	
	private final static String resetAutoIncrement= "ALTER TABLE product AUTO_INCREMENT = 1;";
	
	public static orders findById(int OrderID) {
		orders toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, OrderID);
			rs = findStatement.executeQuery();
			rs.next();

			//int OrderID = rs.getInt("OrderID");
			String customerName = rs.getString("customerName");
			String  productName= rs.getString("productName");
			int productQuantity= rs.getInt("productQuantity");
			
			
				toReturn = new orders (OrderID, customerName, productName, productQuantity);
			
			
				
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public static int insert(orders Order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId=-1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, Order.getCustomerName());
			insertStatement.setString(2, Order.getProductName());
			insertStatement.setInt(3, Order.getQuantity());
			
			Product product = null;
			
			product = ProductDAO.findByName(Order.getProductName());
			
			if(product.getStock()>=Order.getQuantity()){
				 
				ProductDAO.update(product.getID(), null, product.getStock()-Order.getQuantity(), null);
				insertStatement.executeUpdate();

				ResultSet rs = insertStatement.getGeneratedKeys();
				if (rs.next()) {
					insertedId = rs.getInt(1);
					
				}
			
			}
			
			else{
				System.out.println("\nnot enough in stock\n");
				return -1;
			}
			
			
			
			
		
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
	
	
	
	
	
	
	
	public static void update ( int ID, Object updateCustomerName, Object updateProductName, Object updateQuantity)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		orders old=findById(ID);

		try{
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setLong(4,ID);
			if(updateCustomerName==null)
			{
				updateStatement.setString(1,old.getCustomerName());

			}
			else{
				if(CustomerDAO.findByName((String)updateCustomerName)!=null)
					updateStatement.setString(1,(String) updateCustomerName);
				else
				{
					System.out.println("customer inexistent");
					return;
					
				}
			}
			
			if(updateProductName == null)
			{
				updateStatement.setString(2,old.getProductName());

			}
			else{
				
				if(ProductDAO.findByName((String)updateProductName)!=null)
					updateStatement.setString(2,(String) updateProductName);
			
				else
				{
					System.out.println("product inexistent");
					return;
				}
			}
			
			if(updateQuantity==null)
			{
				updateStatement.setInt(3,old.getQuantity());

			}
			else{
				
				if(old.getQuantity()+ProductDAO.findByName((String)updateProductName).getStock()>=(int)updateQuantity){
					updateStatement.setInt(3,(int)updateQuantity);
					ProductDAO.update((ProductDAO.findByName((String)updateProductName)).getID(), null, old.getQuantity()+ProductDAO.findByName((String)updateProductName).getStock() -(int)updateQuantity, null);
				}
				else
					{System.out.println("not enough stock");
					
						return;
					}
			}
			updateStatement.executeUpdate();
		 
		}
		catch(SQLException e)
		{
			LOGGER.log(Level.WARNING, "OrderDAO:update " + e.getMessage());
			
		}
		
		finally{
			
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
			
		}
		
		
	}
	
	
	
	public static void updateIncrement()
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateIncrement =null;
		
		try{
			updateIncrement = dbConnection.prepareStatement(resetAutoIncrement);
			updateIncrement.executeUpdate();

			
			
		}
		 catch(Exception e){}
		finally
		{
			ConnectionFactory.close(updateIncrement);
			ConnectionFactory.close(dbConnection);

			
		}
		
	}

	 
	
	
	
}
	
	

