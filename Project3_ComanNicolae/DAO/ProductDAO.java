package DAO;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import Connection.ConnectionFactory;
 
import GUI.OrderGUI;
import GUI.ProductGUI;
import MODEL.Product;


 
 

public class ProductDAO {

	protected static final Logger LOGGER = Logger.getLogger(Product.class.getName());
	
	private static final String insertStatementString = "INSERT INTO product (productName,productStock,productPrice)"
			+ " VALUES (?,?,?)";
	private static final String deleteStatementString = "DELETE  FROM product where productID = ?";
	
	private final static String findStatementString = "SELECT * FROM product where productID = ?";
	
	private final static String findByNameStatementString="SELECT * FROM product where productName = ?";
	
	private final static String updateStatementString ="UPDATE product SET productName = ?, productStock = ?, productPrice = ? WHERE productID = ?";
	
	private final static String resetAutoIncrement= "ALTER TABLE product AUTO_INCREMENT = 1;";
	
	public static Product findById(int productId) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, productId);
			rs = findStatement.executeQuery();
			rs.next();

			String productName = rs.getString("productName");
			int productStock = rs.getInt("productStock");
			int productPrice = rs.getInt("productPrice");
		 
			toReturn = new Product(productId, productName, productStock, productPrice);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	public static Product findByName(String productName) {
		Product toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findByNameStatementString);
			findStatement.setString(1, productName);
			rs = findStatement.executeQuery();
			rs.next();

			int productID = rs.getInt("productID");
			int productStock= rs.getInt("productStock");
			int productPrice = rs.getInt("productPrice");
		 
			toReturn = new Product(productID, productName, productStock, productPrice);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"ProductDAO:findByName " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}
	
	
	
	public static int insert(Product product) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		
		int insertedId=-1;
	 
		Product aux=findByName(product.getName());

		if(aux!=null)
		{
				update(aux.getID(), null,product.getStock()+aux.getStock(),null);
				insertedId=aux.getID();
				return insertedId;
		}
		
		else
		{
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			insertStatement.setString(1, product.getName());
			insertStatement.setInt(2, product.getStock());
			insertStatement.setInt(3, product.getPrice());
			
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
				
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "ProductDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
		}
		
	}
	
	
	public static void delete(int ID)
	{
		 
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement deleteStatement = null;
		
		
		try{
			deleteStatement = dbConnection.prepareStatement(deleteStatementString);
			deleteStatement.setLong(1,ID);
			deleteStatement.executeUpdate();
			ProductDAO.updateIncrement();
		}
		catch(SQLException e)
		{
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
			
		}
		
		finally{
			
			ConnectionFactory.close(deleteStatement);
			ConnectionFactory.close(dbConnection);
			
		}
	}
	
	public static void update ( int ID, Object updateName, Object updateStock, Object updatePrice)
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateStatement = null;
		
		Product old=findById(ID);

		try{
			updateStatement = dbConnection.prepareStatement(updateStatementString);
			updateStatement.setLong(4,ID);
			if(updateName==null)
			{
				updateStatement.setString(1,old.getName());

			}
			else{
			updateStatement.setString(1,(String) updateName);
			}
			
			if(updateStock == null)
			{
				updateStatement.setInt(2,old.getStock());

			}
			else{
			updateStatement.setInt(2,(int) updateStock);
			}
			
			if(updatePrice==null)
			{
				updateStatement.setInt(3,old.getPrice());

			}
			else{
			updateStatement.setInt(3,(int)updatePrice);
			}
			updateStatement.executeUpdate();
		 
		}
		catch(SQLException e)
		{
			LOGGER.log(Level.WARNING, "ProductDAO:delete " + e.getMessage());
			
		}
		
		finally{
			
			ConnectionFactory.close(updateStatement);
			ConnectionFactory.close(dbConnection);
			
		}
		
		
	}
	
	
	
	public static void updateIncrement()
	{
		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement updateIncrement =null;
		
		try{
			updateIncrement = dbConnection.prepareStatement(resetAutoIncrement);
			updateIncrement.executeUpdate();

			
			
		}
		 catch(Exception e){}
		finally
		{
			ConnectionFactory.close(updateIncrement);
			ConnectionFactory.close(dbConnection);

			
		}
		
	}
	
	public static void fillComboBox()
	{
		Connection dbConnection=ConnectionFactory.getConnection();
		
		String querry="SELECT *  from product";
		ResultSet rs = null;
		try{
		
			Statement statement =dbConnection.createStatement();

			rs =statement.executeQuery(querry);
		
			while(rs.next())
			{
				String name = "productName";
				OrderGUI.productComboBox.addItem(rs.getString(name));
				
			}
		
		}
		catch (SQLException e)
		{
			
			LOGGER.log(Level.WARNING, "ProductDAO:fillComboBox " + e.getMessage());

			
		}
		
		finally{
			
			ConnectionFactory.close(dbConnection);
			
		}
	
	}
	
	public static void fillJTable()
	{
		
		
		Connection dbConnection=ConnectionFactory.getConnection();
		String querry="SELECT *  from product";
		ResultSet rs = null;
		try{
			
			Statement statement =dbConnection.createStatement();

			rs =statement.executeQuery(querry);
		
			while(rs.next())
			{
				int id = rs.getInt("productID");
				String name = rs.getString("productName");
				int stock= rs.getInt("productStock");
				int price = rs.getInt("productPrice");
				ProductGUI.model.addRow(new Object[]{id,name,stock,price});
				
			}
		
		}
		catch (SQLException e)
		{
			
			LOGGER.log(Level.WARNING, "CustomerDAO:fillJtable " + e.getMessage());

			
		}
		
		finally{
			
			ConnectionFactory.close(dbConnection);
			
		}
	
		
	}
	

	 
	
	
	
}
	
	

