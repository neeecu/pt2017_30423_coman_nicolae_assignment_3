package GUI;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;//
import javax.swing.JButton;//
import javax.swing.JFrame;//
import javax.swing.JLabel;//
import javax.swing.JPanel;//
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import BLL.CustomerBLL;
import MODEL.Customer;
import DAO.CustomerDAO;




public class CustomerGUI {
	
	public static DefaultTableModel model= new DefaultTableModel();

	
	
 
	public static void runCustomerGUI()
	{

	
	JFrame mainFrame = new JFrame("Customer");
	
	mainFrame.setVisible(true);
	mainFrame.setSize(1000,600);
	mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	
    JButton showALL = new JButton("View all");
    JButton backButton = new JButton("Back");
    
    JTable tabelc = new JTable(CustomerGUI.model);
    JTable CustomerTable= new JTable();
	CustomerTable.setEnabled(false);
    
    JPanel mainPanel = new JPanel();
    JPanel secondPanel = new JPanel();
    JPanel firstPanel = new JPanel(new BorderLayout());
    JPanel thirdPanel = new JPanel ();
   
    CustomerTable.setSize(300, 300);
    CustomerTable=tabelc;
    
    JLabel l1 = new JLabel("customerID");
    JLabel l2 = new JLabel("customerName");
    JLabel l3 = new JLabel("CustomerEmail");
    JLabel l4 = new JLabel ("CustomerAddress");
    
    JTextField tf1 = new JTextField(),tf2=new JTextField(),tf3=new JTextField(),tf4= new JTextField();
    
    tf1.setColumns(10);
    tf2.setColumns(10);
    tf3.setColumns(10);
    tf4.setColumns(10);
    
    JPanel p1= new JPanel();
    p1.setLayout(new FlowLayout());
    p1.add(l1);
    p1.add(tf1);
    
    JPanel p2= new JPanel();
    p2.setLayout(new FlowLayout());
    p2.add(l2);
    p2.add(tf2);
    
    JPanel p3= new JPanel();
    p3.setLayout(new FlowLayout());
    p3.add(l3);
    p3.add(tf3);
    
    JPanel p4= new JPanel();
    p4.setLayout(new FlowLayout());
    p4.add(l4);
    p4.add(tf4);
    
    JButton insert= new JButton("Insert"), delete= new JButton("Delete"), update = new JButton("Update");
    
    
    
    
    
    
    firstPanel.add(backButton,BorderLayout.NORTH);
    firstPanel.add(new JScrollPane(CustomerTable),BorderLayout.CENTER);
    firstPanel.add(showALL,BorderLayout.SOUTH);
    firstPanel.setSize(300,300);
 
    BoxLayout boxlayout = new BoxLayout(secondPanel, BoxLayout.Y_AXIS);
    secondPanel.setLayout(boxlayout);
    secondPanel.add(p1);
    secondPanel.add(p2);
    secondPanel.add(p3);
    secondPanel.add(p4);
    
 
    thirdPanel.setLayout(new BoxLayout(thirdPanel, BoxLayout.Y_AXIS));
    thirdPanel.add(insert);
    thirdPanel.add(delete);
    thirdPanel.add(update);
    
    mainPanel.setLayout(new FlowLayout());
    mainPanel.add(firstPanel);
    mainPanel.add(secondPanel);
    mainPanel.add(thirdPanel);
    
    mainFrame.add(mainPanel);
    
    
	 
			
			 class  Handler  implements ActionListener 
		      {  
		           
		    	  public void actionPerformed(ActionEvent e){
		    		  
		    		  
		    		  if(e.getSource()==showALL)
		    		  {
		    			   
		    			  CustomerGUI.model.setRowCount(0);
		    			  CustomerDAO.fillJTable();
		    			  mainFrame.repaint();
		    			  tabelc.repaint();
		    			  
		    		  }
		    		  
		    		  if(e.getSource()==backButton)
		    		  {
		    			  mainFrame.dispose();
		    			  GUI.menuFrame.setVisible(true);
		    			  
		    		  }
		    		  
		    		  if(e.getSource()==insert)
		    		  {
		    			String  customerNameString=tf2.getText();
		    			String 	customerAddressString=tf4.getText();
		    			String  customerEmail = tf3.getText();
		    			
		    			Customer c1 = new Customer ( customerNameString, customerEmail, customerAddressString);
		    			CustomerBLL bll = new CustomerBLL();
		    			bll.insertCustomer(c1);
		    			tabelc.repaint();
		    			  
		    		  }
		    		  
		    		  if(e.getSource()==delete)
		    		  {
		    			  	int id =Integer.parseInt(tf1.getText());
		    			  	 
 			    			CustomerBLL bll = new CustomerBLL();
			    			bll.deleteCustomer(id);
			    			tabelc.repaint();
		    			  
		    			  
		    		  }
		    		  
		    		 if(e.getSource()==update)
		    		 {
		    			 
		    			 CustomerBLL bll = new CustomerBLL();
		    			int id =Integer.parseInt(tf1.getText());
		    			String  customerNameString=tf2.getText();
			    		String 	customerAddressString=tf4.getText();
			    		String  customerEmail = tf3.getText();
			    		
			    		if(tf2.getText().isEmpty())
			    			customerNameString=DAO.CustomerDAO.findById(id).getName();
			    		
			    		if(tf4.getText().isEmpty())
			    			customerAddressString=DAO.CustomerDAO.findById(id).getAddress();
			    		
			    		if(tf3.getText().isEmpty())
			    			customerEmail=DAO.CustomerDAO.findById(id).getEmail();
			    		
			    		
		    			bll.updateCustomer(id, customerNameString, customerAddressString, customerEmail);
			    		
		    		 }
		    		  
		    		  
		    	  }
	
		      }
			 
	Handler handler= new Handler();
	showALL.addActionListener(handler);
	backButton.addActionListener(handler);
	insert.addActionListener(handler);
	delete.addActionListener(handler);
	update.addActionListener(handler);
	}


}
