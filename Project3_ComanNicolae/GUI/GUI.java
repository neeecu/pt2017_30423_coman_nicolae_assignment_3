package GUI;
 
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;//
import javax.swing.JButton;//
import javax.swing.JFrame;//
import javax.swing.JLabel;//
import javax.swing.JPanel;//
import javax.swing.JPasswordField;
 
import javax.swing.JTextField;
 


public abstract class GUI {

	
	static JFrame menuFrame= new JFrame("Tables"); 

	
	public  GUI() 
	{
		createGUI();
	}
	
	
	public static void createGUI(){
		
		JFrame Login = new JFrame("Authentification");
		Login.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Login.setSize(350,150);
		
		JPanel user = new JPanel();
		JPanel pass = new JPanel();
		JPanel button = new JPanel();
		JPanel LoginPanels = new JPanel();
		
		JLabel username = new JLabel("username");
		JLabel password = new JLabel("password");
		
		JTextField usernameTextField = new JTextField();
		usernameTextField.setColumns(10);
		
		JPasswordField passTextField = new JPasswordField();
		passTextField.setColumns(10);
		
		JButton loginButton = new JButton("login");
		JButton exit = new JButton("exit");
		
		user.add(username);
		user.add(usernameTextField);
		user.setLayout(new FlowLayout());
		
		pass.add(password);
		pass.add(passTextField);
		pass.setLayout(new FlowLayout());
		
		button.add(loginButton);
		button.setLayout(new FlowLayout());
		
		 LoginPanels.setLayout(new BoxLayout(LoginPanels,BoxLayout.Y_AXIS));
		
		 LoginPanels.add(user);
		 LoginPanels.add(pass);
		 LoginPanels.add(button);
		 
		
		 Login.setContentPane(LoginPanels);
		 Login.setVisible(true);
////////////////////////////////////////////////
		 //MENU  FRAME
		 
		  JPanel MainPanel=new JPanel();

		 
		  JButton client = new JButton("Client");
		  JButton order  = new JButton("Order");
		  JButton product = new JButton("Product");
		
		  
 
		 
///////////////////////////////
		  //////////// CUSTOMER TABLE
		  
			CustomerGUI.model.addColumn("customerID");
			CustomerGUI.model.addColumn("customerName");
			CustomerGUI.model.addColumn("customerAddress");
			CustomerGUI.model.addColumn("customerEmail");
			
/////////////////////////////////////////////
			/////////////////////////PRODUCT TABLE
			ProductGUI.model.addColumn("productID");
			ProductGUI.model.addColumn("productName");
			ProductGUI.model.addColumn("productStock");
			ProductGUI.model.addColumn("productPrice");

		 
		 
		 class  Handler  implements ActionListener 
	      {  
	           
	    	  public void actionPerformed(ActionEvent e){
	    		  
	    	  if(e.getSource()==loginButton)
	    	  {
	    		  String userString =usernameTextField.getText();
	    		  
	    		  
				String passString =new String(passTextField.getPassword());
	    		 
				 
				
	    		  if(userString.equals(Connection.ConnectionFactory.GetUser()) && passString.equals(Connection.ConnectionFactory.GetPass()))
	    		  {
	    			  
	    			  Login.dispose();
	    			 
	    			  menuFrame.setVisible(true);
	    			  menuFrame.setSize(300, 150);
	    			  
	    			  
	    			  JPanel Panel1 = new JPanel();
	    			  JPanel Panel2 = new JPanel();
	    			  
	    			  Panel1.setLayout(new FlowLayout());
	    			  
	    			  Panel2.setLayout(new FlowLayout());
	    			  Panel2.add(exit);
	    			  
	    			  
	    			  
	    			  Panel1.add(client);
	    			  Panel1.add(order);
	    			  Panel1.add(product);
	    			  
	    			  
	    			  MainPanel.add(Panel1);
	    			  MainPanel.add(Panel2);
	    			  MainPanel.setLayout(new BoxLayout(MainPanel, BoxLayout.Y_AXIS));
	    			  
	    			  menuFrame.setContentPane(MainPanel);
	    			  
	    			  
	    		  }
	    		  
	    	  }
	    		  
	    	  if(e.getSource()==client)
	    	  {
	    		  
	    		  menuFrame.setVisible(false);
	    		  CustomerGUI.runCustomerGUI();
	    		  
	    	  }
	    	  
	    	  
	    	  if(e.getSource()==order)
	    	  {
	    		  
	    		  menuFrame.setVisible(false);
	    		  OrderGUI.runOrderGUI();

	    	  }
	    	  
	    if(e.getSource()==product)
	    {
	    	menuFrame.setVisible(false);
	    	 ProductGUI.runProductGUI();
	    }
 
	    
	    if(e.getSource()==exit)
	    {
	    	menuFrame.dispose();	    	
	
	    }
	    	  
	   }
		 
		 

 }
		 
		 Handler handler = new Handler();
		loginButton.addActionListener(handler);
		client.addActionListener(handler);
		order.addActionListener(handler);
		product.addActionListener(handler);
		exit.addActionListener(handler);
	}
}