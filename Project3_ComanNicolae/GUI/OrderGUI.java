package GUI;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;//
import javax.swing.JButton;//
import javax.swing.JFrame;//
import javax.swing.JLabel;//
import javax.swing.JPanel;//
 
 
import javax.swing.JTextField;
 
import BLL.OrderBLL;
import DAO.ProductDAO;
import MODEL.orders;

import javax.swing.JComboBox;
 

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Date;

import com.itextpdf.text.Document;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;
 

public class OrderGUI {
	
	public static JComboBox customerComboBox = new JComboBox();
	
	public static JComboBox productComboBox = new JComboBox();
	static String customerNameString;
	static String productNameString;
	static int    quantityInt = 0;
	
	 

	public static void runOrderGUI()
	{
		
		
		
		 JButton createOrder = new JButton("Order!");
		 JFrame OrderFrame = new JFrame("Order");
		

		  JButton backButtonOrder = new JButton("Back");

		  
		  JLabel customerLabel = new JLabel("customer");
		  JLabel productLabel = new JLabel("product");
		  JLabel quanityLabel = new JLabel("quantity");
		  
		  JTextField quantityTextField = new JTextField();
		  quantityTextField.setColumns(10);
		  
		  
		  JPanel customerPanel= new JPanel();
		  JPanel productPanel = new JPanel();
		  JPanel backButtonPanel=new JPanel();
		  JPanel quantityPanel=new JPanel();
		  JPanel createOrderPanel = new JPanel();
		
		  OrderFrame.setVisible(true);
		  OrderFrame.setSize(500, 300);
		  
		  
		  JPanel mainPanel = new JPanel();
		  
		  JPanel auxPanel = new JPanel();
		  auxPanel.setLayout(new BoxLayout(auxPanel,BoxLayout.X_AXIS));
		  
		  JPanel auxPanel2 = new JPanel();
		  auxPanel2.setLayout(new BoxLayout(auxPanel2,BoxLayout.X_AXIS));
		  
		  
		  DAO.CustomerDAO.fillComboBox();
		  DAO.ProductDAO.fillComboBox();
		  
		  customerPanel.add(customerLabel);
		  customerPanel.add(customerComboBox);
		  customerPanel.setLayout(new FlowLayout());
		  
		  productPanel.add(productLabel);
		  productPanel.add(productComboBox);
		  productPanel.setLayout(new FlowLayout());
		  
		  quantityPanel.add(quanityLabel);
		  quantityPanel.add(quantityTextField);
		  quantityPanel.setLayout(new FlowLayout());
		  
		  backButtonPanel.setLayout(new FlowLayout());
		  backButtonPanel.add(backButtonOrder);
		  
		  createOrderPanel.add(createOrder);
		  createOrderPanel.setLayout(new FlowLayout());
		  
		  auxPanel.add(customerPanel);
		  auxPanel.add(productPanel);
		  auxPanel2.add(createOrderPanel);
		  auxPanel2.add(backButtonPanel);
		  
		  mainPanel.add(auxPanel);
		  mainPanel.add(quantityPanel);
		  mainPanel.add(auxPanel2);
		  mainPanel.setLayout(new BoxLayout(mainPanel,BoxLayout.Y_AXIS));
		  
		  
		  OrderFrame.setContentPane(mainPanel);
		
		 
		 
		 class  Handler  implements ActionListener 
	      {  
	           
	    	  public void actionPerformed(ActionEvent e){
	    		  
	    		  if(e.getSource()==backButtonOrder){
	    			  
	    			  GUI.menuFrame.setVisible(true);
	    			  OrderFrame.dispose();
 	    		  }
	    		  
		
	    		  
	    		  if(e.getSource() == createOrder)
	    		  {
	    			  quantityInt=Integer.parseInt(quantityTextField.getText());
	    			  customerNameString=(String)customerComboBox.getSelectedItem();
	    			  productNameString=(String)productComboBox.getSelectedItem();
	    			  orders order= new orders(customerNameString,productNameString,quantityInt);
	    			  OrderBLL orderbll=new OrderBLL();
	    			  
	    			int id=  orderbll.insertOrder(order);
	    			  File file2=new File("C:\\Users\\neecu\\workspace\\Project3_Coman_Nicolae\\"+customerNameString+" Bill.pdf");

	    		try{  
	    			if(id>0)
	    			{
	    			
	    			  OutputStream file = new FileOutputStream(file2);
	    			  Document document = new Document();
	    			    
	    			PdfWriter.getInstance(document, file);
 	    			document.open();
	    			document.add(new Paragraph(new Date().toString()));
	    			document.add(new Paragraph("\n The Client "+ customerNameString+" has just realized an order consisting of " +quantityInt + " " +" pices of " + productNameString +", having to pay the sum of " + ProductDAO.findByName(productNameString).getPrice()*quantityInt));
    			    
	    			document.addTitle(" "+customerNameString+" Bill");
	    			document.close();

	    			file.close();
	    			
	    			if (Desktop.isDesktopSupported())
	    	        	Desktop.getDesktop().open(file2);

	    			}
	    			else {}
	    		}
	    		
	    			catch(Exception excep)
	    			{
	    				
	    		 	excep.printStackTrace();
	    			}
	    		  }
	    	  }
	      }
		 Handler handler= new Handler();
		 backButtonOrder.addActionListener(handler);
		 createOrder.addActionListener(handler);
	}


}
