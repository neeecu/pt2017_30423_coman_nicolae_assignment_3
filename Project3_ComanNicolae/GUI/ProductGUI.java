package GUI;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;//
import javax.swing.JButton;//
import javax.swing.JFrame;//
import javax.swing.JLabel;//
import javax.swing.JPanel;//
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import BLL.CustomerBLL;
import BLL.ProductBLL;
import MODEL.Customer;
import MODEL.Product;
import DAO.CustomerDAO;
import DAO.ProductDAO;




public class ProductGUI {
	
	public static DefaultTableModel model= new DefaultTableModel();

	
	
	 

	public static void runProductGUI()
	{

	
	JFrame mainFrame = new JFrame("Product");
	
	mainFrame.setVisible(true);
	mainFrame.setSize(1000,600);
	mainFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

	
    JButton showALL = new JButton("View all");
    JButton backButton = new JButton("Back");
    
    JTable tabelp = new JTable(ProductGUI.model);
    JTable ProductTable= new JTable();
	ProductTable.setEnabled(false);
    
    JPanel mainPanel = new JPanel();
    JPanel secondPanel = new JPanel();
    JPanel firstPanel = new JPanel(new BorderLayout());
    JPanel thirdPanel = new JPanel ();
   
    ProductTable.setSize(300, 300);
    ProductTable=tabelp;
    
    JLabel l1 = new JLabel("productID");
    JLabel l2 = new JLabel("productName");
    JLabel l3 = new JLabel("productStock");
    JLabel l4 = new JLabel ("ProductPrice");
    
    JTextField tf1 = new JTextField(),tf2=new JTextField(),tf3=new JTextField(),tf4= new JTextField();
    
    tf1.setColumns(10);
    tf2.setColumns(10);
    tf3.setColumns(10);
    tf4.setColumns(10);
    
    JPanel p1= new JPanel();
    p1.setLayout(new FlowLayout());
    p1.add(l1);
    p1.add(tf1);
    
    JPanel p2= new JPanel();
    p2.setLayout(new FlowLayout());
    p2.add(l2);
    p2.add(tf2);
    
    JPanel p3= new JPanel();
    p3.setLayout(new FlowLayout());
    p3.add(l3);
    p3.add(tf3);
    
    JPanel p4= new JPanel();
    p4.setLayout(new FlowLayout());
    p4.add(l4);
    p4.add(tf4);
    
    JButton insert= new JButton("Insert"), delete= new JButton("Delete"), update = new JButton("Update");
    
    
    
    
    
    
    firstPanel.add(backButton,BorderLayout.NORTH);
    firstPanel.add(new JScrollPane(ProductTable),BorderLayout.CENTER);
    firstPanel.add(showALL,BorderLayout.SOUTH);
    firstPanel.setSize(300,300);
 
    BoxLayout boxlayout = new BoxLayout(secondPanel, BoxLayout.Y_AXIS);
    secondPanel.setLayout(boxlayout);
    secondPanel.add(p1);
    secondPanel.add(p2);
    secondPanel.add(p3);
    secondPanel.add(p4);
    
 
    thirdPanel.setLayout(new BoxLayout(thirdPanel, BoxLayout.Y_AXIS));
    thirdPanel.add(insert);
    thirdPanel.add(delete);
    thirdPanel.add(update);
    
    mainPanel.setLayout(new FlowLayout());
    mainPanel.add(firstPanel);
    mainPanel.add(secondPanel);
    mainPanel.add(thirdPanel);
    
    mainFrame.add(mainPanel);
    
    
	 
			
			 class  Handler  implements ActionListener 
		      {  
		           
		    	  public void actionPerformed(ActionEvent e){
		    		  
		    		  
		    		  if(e.getSource()==showALL)
		    		  {
		    			   
		    			  ProductGUI.model.setRowCount(0);
		    			  ProductDAO.fillJTable();
		    			  mainFrame.repaint();
		    			  tabelp.repaint();
		    			  
		    		  }
		    		  
		    		  if(e.getSource()==backButton)
		    		  {
		    			  mainFrame.dispose();
		    			  GUI.menuFrame.setVisible(true);
		    			  
		    		  }
		    		  
		    		  if(e.getSource()==insert)
		    		  {
		    			String  productNameString=tf2.getText();
		    			int 	productStock=Integer.parseInt(tf3.getText());
		    			int  productPrice = Integer.parseInt(tf4.getText());
		    			
		    			Product p1 = new Product ( productNameString, productStock, productPrice);
		    			ProductBLL bll = new ProductBLL();
		    			bll.insertProduct(p1);
		    			tabelp.repaint();
		    			  
		    		  }
		    		  
		    		  if(e.getSource()==delete)
		    		  {
		    			  	int id =Integer.parseInt(tf1.getText());
		    			  	 
 			    			ProductBLL bll = new ProductBLL();
			    			bll.deleteProduct(id);
			    			tabelp.repaint();
		    			  
		    			  
		    		  }
		    		  
		    		 if(e.getSource()==update)
		    		 {
		    			 
		    			 ProductBLL bll = new ProductBLL();
		    			int id =Integer.parseInt(tf1.getText());
		    			String  productNameString;
		    			int 	productStock;
		    			int  productPrice ;
			    		
			    		
		    			if(tf2.getText().isEmpty())
			    			productNameString=ProductDAO.findById(id).getName();
			    		else
			    			  productNameString=tf2.getText();
			    		
			    	
			    		if(tf4.getText().isEmpty())
			    			productPrice=ProductDAO.findById(id).getPrice();
			    		else
			    			productPrice= Integer.parseInt(tf4.getText());
			    	
			    		if(tf3.getText().isEmpty())
			    			productStock=ProductDAO.findById(id).getStock();
			    		else
			    			productStock=Integer.parseInt(tf3.getText());
			    		
		    			bll.updateProduct(id, productNameString, productStock, productPrice);
			    		
		    		 }
		    		  
		    		  
		    	  }
	
		      }
			 
	Handler handler= new Handler();
	showALL.addActionListener(handler);
	backButton.addActionListener(handler);
	insert.addActionListener(handler);
	delete.addActionListener(handler);
	update.addActionListener(handler);
	}


}
