package MODEL;

public class Customer {
	
	private int customerID;
	private String customerName;
	private String customerEmail;
	private String customerAddress;



public Customer(int CustomerID, String customerName, String CustomerEmail, String CustomerAddress) {
	super();
	this.customerID = CustomerID;
	this.customerName = customerName;
	this.customerAddress = CustomerAddress;
	this.customerEmail = CustomerEmail;

}



public Customer( String customerName, String CustomerEmail, String CustomerAddress) {
	super();

	this.customerName = customerName;
	this.customerAddress = CustomerAddress;
	this.customerEmail = CustomerEmail;

}


public int getId() {
	return customerID;
}

public void setId(int id) {
	this.customerID = id;
}

public String getName() {
	return customerName;
}

public void setName(String name) {
	this.customerName = name;
}

public String getAddress() {
	return customerAddress;
}

public void setAddress(String address) {
	this.customerAddress = address;
}

 
public String getEmail() {
	return customerEmail;
}

public void setEmail(String email) {
	this.customerEmail = email;
}

@Override
public String toString() {
	return "Customer [id=" + customerID + ", name=" + customerName + ", address=" + customerAddress + ", email=" + customerEmail +  "]";
}

}



