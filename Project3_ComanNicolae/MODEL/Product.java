package MODEL;

public class Product {

	private int productID;
	private String productName;
	private int productStock;
	private int productPrice;
		
	
	public Product(int productID, String productName, int productStock, int productPrice){
		
		this.productID=productID;
		this.productName=productName;
		this.productStock=productStock;
		this.productPrice=productPrice;
		
		
	}
	
	public Product(String productName, int productStock, int productPrice){
		
		this.productName=productName;
		this.productStock=productStock;
		this.productPrice=productPrice;
		
	}
	
	public int getID(){
		
		return productID;
		
		
	}
	
	public void setID(int ID){
	
	this.productID=ID;
	}
	
	public String getName(){
		
		
		return this.productName;
		
	}
	
	public void setName( String name){
		
		
		this.productName=name;
		
	}
	
	public int getStock(){
	
		return this.productStock;
	}
	
	
	public void setStock(int stock){
		
		
		this.productStock=stock;
		
	}
	
	
	public int getPrice(){
		return this.productPrice;
		
	}
	
	public void setPrice( int price){
		
		this.productPrice=price;
		
	}
	
	
	
}
