package MODEL;

public class orders {

	private int orderID;
	private String customerName;
	private String productName;
	private int productQuantity;
	
	
	
	public orders( int orderID, String customerName, String productName, int quantity){
		
		this.orderID=orderID;
		this.customerName=customerName;
		this.productName=productName;
		this.productQuantity=quantity;
		
	}
	

	public orders(  String customerName, String productName, int quantity){
		
		
		this.customerName=customerName;
		this.productName=productName;
		this.productQuantity=quantity;
		
	}
	
	public void setOrderID(int id)	{
		
		this.orderID=id;
		
	}
	
	public int getOrderID(){
			
		return this.orderID;	
	}

public void setCustomerName(String name)	{
		
		this.customerName=name;
		
	}
	
	public String getCustomerName(){
			
		return this.customerName;	
	}

public void setProductName(String name)	{
		
		this.productName=name;
		
	}
	
	public String getProductName(){
			
		return this.productName;	
	}
	
	
	public void setQuantity(int quantity){
		
		this.productQuantity=quantity;
		
	}

	public int getQuantity(){
		
		return this.productQuantity;
	}

	
}

